import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home.component';

import { ROUTES } from './home.routes';

@NgModule({
  imports: [
    CommonModule,
    ROUTES
  ],
  declarations: [HomeComponent]
})
export class HomeModule { }
